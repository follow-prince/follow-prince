<div align="center">
<h2><h1><img src="./assets/hello.svg" width="400"/> <br> 𝗜'𝗺 ELAVARASAN <img src="https://media.giphy.com/media/12oufCB0MyZ1Go/giphy.gif" width="50"></h1></h2>
<p align="center"><em> Software Developer
  </a><img src="https://media.giphy.com/media/WUlplcMpOCEmTGBtBW/giphy.gif" width="40"> 
  </em></p> 

    


  <a href="https://github.com/follow-prince/">
    <img alt="total stars" title="Total stars on GitHub" src="https://custom-icon-badges.demolab.com/github/stars/follow-prince?color=55960c&style=for-the-badge&labelColor=488207&logo=star"/></a>
  <a href="https://github.com/follow-prince?tab=followers">
    <img alt="followers" title="Follow me on Github" src="https://custom-icon-badges.demolab.com/github/followers/follow-prince?color=236ad3&labelColor=1155ba&style=for-the-badge&logo=person-add&label=Followers&logoColor=white"/></a>
  <a href="https://github.com/follow-prince/view-count-badge">
    <img alt="views" title="GitHub profile views" src="https://view-count-badge.zohan.tech/follow-prince/profile?color=6b105d&labelColor=913e96&style=for-the-badge&logo=eye&label=VISITORS&logoColor=white"/></a> 
  </p>

 [![Instagram](https://img.shields.io/badge/follow-prince-%23E4405F.svg?style=for-the-badge&logo=Instagram&logoColor=white)](https://www.instagram.com/follow.prince/)&nbsp;&nbsp;
[![Linkedin](https://img.shields.io/badge/follow-elavarasa003-%231DA1F2.svg?style=for-the-badge&logo=Linkedin&logoColor=white)](https://www.linkedin.com/in/elavarasa003/)&nbsp;&nbsp; [![Twitter](https://img.shields.io/badge/Follow-Prince-1DA1F2?style=for-the-badge&logo=twitter&logoColor=white)](https://twitter.com/intent/follow?screen_name=_follow_prince)&nbsp;&nbsp;  <br>
   <a href="https://www.buymeacoffee.com/follow.prince" target="_blank"><img src="https://cdn.buymeacoffee.com/buttons/default-orange.png" alt="Buy Me A Coffee"  width="100" style="border-radius:150px" />   <a href='https://ko-fi.com/follow_prince' target='_blank'><img width="95" src='https://cdn.ko-fi.com/cdn/kofi3.png?v=2' alt='Buy Coffee for Prince' />
  </a>
    


## GitHub Profile Trophy 🏆
[![trophy](https://github-profile-trophy.vercel.app/?username=follow-prince&theme=onedark)](https://github.com/follow-prince/github-profile-trophy) 


<h2> Who I am 🤔❓</h2>
    <p>Hi there! I'm a student, developer, and  volunteer. I believe that personal growth and learning are essential for success, and I have dedicated my life to achieving both. Because of that, I'm always looking to learn new things and apply them to whatever I'm doing. Technology is always igniting my passion for the positive impact it can have, and I'm excited to continue growing my skills and knowledge, so I started my career as a techie. In addition to my studies, I'm also passionate about giving back to various communities and helping others, which is why I volunteer my time to help out whenever I can, and I believe that this work is vital in helping to create a better future for everyone.  
</p>

## Github stats 📊 

<a href="https://wakatime.com/@6817c547-15ff-41d8-be95-49f82f64eb1e"><img src="https://wakatime.com/badge/user/6817c547-15ff-41d8-be95-49f82f64eb1e.svg" alt="Total time coded since Jul 28 2023" /></a>




<a href="https://elavarasan.me" target="_blank" title="GitHub metrics!">
        <img width="400" src="https://raw.githubusercontent.com/follow-prince/follow-prince/main/assets/gen/metrics.svg" />
    </a>  <a href="https://app.daily.dev/follow-prince"><img src="https://api.daily.dev/devcards/da85f8f5e0b24aa2976eccb7081e709f.png?r=lgc" width="250" alt="ELAVARASAN S's Dev Card"/></a>

![](http://github-profile-summary-cards.vercel.app/api/cards/stats?username=follow-prince&theme=github_dark)
![](http://github-profile-summary-cards.vercel.app/api/cards/repos-per-language?username=follow-prince&theme=github_dark)

  <img src="profile-3d-contrib/profile-night-rainbow.svg" width="800" style=" border-radius: 20px;"  >

  <a href="https://elavarasan.me" target="_blank" title="Click Here..!">
    <img width="700" src="https://raw.githubusercontent.com/follow-prince/follow-prince/main/assets/gen/snake.svg" />
  </a>



<table>

## Bio
<tr><td>🤓 <b>Name</b></td><td>ELAVARASAN</td></tr>
<tr><td> 😘 <b>Nickname</b></td><td>Prince</td></tr>
        <tr><td>💼 <b>Career</b></td><td> Student |  Developer |  Volunteer</td></tr>
        <tr><td>🎒 <b>Campus</b></td><td><a href="https://www.drmgrdu.ac.in/">Dr. M.G.R. Educational And  Research Institute - Chennai</a></td></tr>
        <tr><td>🎓 <b>Course</b></td><td><a href="https://www.nibm.lk/programmes/bsc-hons-computing/">MCA - Master of Computer Applications</a></td></tr>
        <tr><td>🌐 <b>Domain</b></td><td><a href="https://elavarasan.me">www.elavarasan.me</a></td></tr>
        <tr><td>🌱 <b>Techie v1</b></td><td>2022</td></tr>
        <tr><td>🚀 <b>Techie v2</b></td><td>2023</td></tr>
        <tr><td>💫 <b>Focus On</b></td><td><a href="https://flutter.dev">Flutter</a> | <a href="https://dart.dev">Dart</a> | <a href="https://firebase.google.com">Firebase</a> | <a href="https://g.dev/follow-prince">Machine Learning</a> | <a href="https://www.python.org/">Python</a></td></tr>
        <tr><td>📄 <b>Blog</b></td><td>View my blog here 👉🏽 <a href="https://i-am-prince.vercel.app"> blog.prince.dev</a></td></tr>
        <tr><td>📌 <b>Gists</b></td><td><a href="https://gist.github.com/follow-prince">GitHub Gist</a></td></tr>
        <tr><td>🧙 <b>Support</b></td><td>Open Source | Volunteering | Helping Others | Peace | Loneliness</td></tr>
        <tr><td>💖 <b>Hobbies</b></td><td>Learning New Things | Listening to Music | Content Creation</td></tr>
        <tr><td>😍 <b>Love</b></td><td>To Make New Friends</td></tr>
        <tr><td>🌟 <b>Excellent Lines</b></td><td>வாய்ப்புகளை தேடி அலையாதே, <br> வாய்ப்புகளை உருவாக்கு...💯✨</td></tr>
    </table> <h2>Where you can find me 😉🔍</h2>
    <a href="https://hashnode.com/@elavarasan"><img src="https://ghmd.dileepabandara.dev/widgets/blog/dark/hashnode.png" height=50px weight=50px></img></a> &nbsp
    <a href="https://www.linkedin.com/in/elavarasa003/"><img src="https://ghmd.dileepabandara.dev/widgets/social/dark/linkedin.png" height=50px weight=50px></img></a> &nbsp
    <a href="https://twitter.com/follow_prince_"><img src="https://ghmd.dileepabandara.dev/widgets/social/dark/twitter.png" height=50px weight=50px></img></a> &nbsp
    <a href="https://www.youtube.com/@follow-prince"><img src="https://ghmd.dileepabandara.dev/widgets/social/dark/youtube.png" height=50px weight=50px></img></a> &nbsp
    <a href="https://t.me/follow-prince"><img src="https://ghmd.dileepabandara.dev/widgets/social/dark/telegram.png" height=50px weight=50px></img></a> &nbsp
    <a href="https://www.facebook.com/elavarasa003"><img src="https://ghmd.dileepabandara.dev/widgets/social/dark/facebook.png" height=50px weight=50px></img></a> &nbsp
    <a href="https://instagram.com/follow.prince"><img src="https://ghmd.dileepabandara.dev/widgets/social/dark/instagram.png" height=50px weight=50px></img></a> &nbsp
    <!-- <a href="https://wa.me/message/Q5YS5QJOEG6RO1"><img src="https://ghmd.dileepabandara.dev/widgets/social/dark/whatsapp.png" height=50px weight=50px></img></a> &nbsp  -->
    </br></br>
    <b>View my all profiles here 👉🏽</b> <a href="https://profile-prince-dev.vercel.app/"> profile.prince.dev</a>
    <h2>How you can contact me 😋📧</h2>
    <p>Hey! I appreciate you looking at my public profile. Please feel free to ask me anything or to discuss anything with me. I'm happy to chat with and meet new people. Regardless of the fact that I don't use social media much, you can find me there. So find me and get in touch with me. Send me an email if you need to discuss anything serious with me. I'll get back to you in 12 to 24 hours.</p>
    <b>Send me an email at 👉🏽</b> <a href="mailto:elavarasa.003@gmail.com"> elavarasa.003@gmail.com</a> | <a href="mailto:contact@elavarasan.me"> contact@elavarasan.me</a>

     
 ## Support me guys
 <a href="https://www.buymeacoffee.com/follow.prince" target="_blank"><img src="https://cdn.buymeacoffee.com/buttons/default-orange.png" alt="Buy Me A Coffee"  width="200" style="border-radius:150px" />   <a href='https://ko-fi.com/follow_prince' target='_blank'><img width="200" src='https://cdn.ko-fi.com/cdn/kofi3.png?v=2' alt='Buy Coffee for Prince' />
  </a>

</div>


